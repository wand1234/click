package ru.test.click.ui.fragment.base;

import android.support.v4.app.Fragment;

import org.androidannotations.annotations.EBean;

@EBean
public abstract class BaseFragment extends Fragment {
}
