package ru.test.click.ui.animations;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

public class ClickAnimation extends AlphaAnimation {

    /**
     * Ранейбл для выполнения операций после анимации
     */
    private Runnable runnable;

    public ClickAnimation() {
        //Создаем анимацию
        super(0.3f, 1.0f);

        //Время нажатия 200 мс
        setDuration(200);

        //Листенер анимации
        setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //Если задан ранейбл то выполняем его
                if (runnable != null)
                    runnable.run();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * Выполнить что либо после выполнения анимации
     * @param runnable ранейбл
     * @return текущий объект
     */
    public ClickAnimation setEndRunnable(Runnable runnable) {
        this.runnable = runnable;
        return this;
    }
}
