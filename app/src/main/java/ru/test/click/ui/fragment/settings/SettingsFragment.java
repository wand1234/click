package ru.test.click.ui.fragment.settings;

import android.view.View;
import android.widget.EditText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.math.BigDecimal;

import ru.test.click.R;
import ru.test.click.ui.animations.ClickAnimation;
import ru.test.click.ui.fragment.base.BaseFragment;

@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends BaseFragment {

    @Bean(SettingsPresenterImp.class)
    protected SettingsPresenterImp presenter;

    @ViewById(R.id.step_value)
    protected EditText stepValue;

    @ViewById(R.id.max_value)
    protected EditText maxValue;

    @AfterViews
    protected void show() {
        //Устанавливаем представление
        presenter.setView(this);

        //Выводим настройки
        presenter.refresh();
    }

    /**
     * Получаем значение шага из формы
     * @return значение шага
     */
    public BigDecimal getStepValue() {
        if (stepValue.getText().length() == 0) return null;
        return new BigDecimal(stepValue.getText().toString());
    }

    /**
     * Получаем максимальное значение
     * @return значение
     */
    public BigDecimal getMaxValue() {
        if (maxValue.getText().length() == 0) return null;
        return new BigDecimal(maxValue.getText().toString());
    }

    /**
     * Устанавливаем значение шага
     * @param value новое значение
     */
    public void setStepValue(BigDecimal value) {
        stepValue.setText(value.toString());
    }

    /**
     * Устанавливаем максимальное значение
     * @param value новое значение
     */
    public void setMaxValue(BigDecimal value) {
        maxValue.setText(value.toString());
    }

    @Click(R.id.save)
    protected void save() {
        presenter.save();
    }

    @Click(R.id.reset)
    protected void reset() {
        presenter.reset();
    }

    @Click(R.id.back_button)
    protected void back(View view) {
        view.startAnimation(
                new ClickAnimation().setEndRunnable(() -> presenter.back())
        );
    }
}
