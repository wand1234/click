package ru.test.click.ui.fragment.click;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.math.BigDecimal;

import ru.test.click.R;
import ru.test.click.preferences.shared.SharedPreferences;
import ru.test.click.settings.DefaultSettings;
import ru.test.click.ui.activity.base.BaseActivity;
import ru.test.click.ui.fragment.settings.SettingsFragment_;

@EBean
public class ClickPresenterImp {

    @RootContext
    protected BaseActivity activity;

    /**
     * Представление
     */
    private ClickFragment view;

    @Bean
    protected SharedPreferences preferences;

    /**
     * Текущее значение, кеш
     */
    private BigDecimal value;

    /**
     * Шаг, кеш
     */
    private BigDecimal stepValue;

    /**
     * Максимальное значение, кеш
     */
    private BigDecimal maxValue;

    /**
     * Устанавливаем представление
     * @param view представление
     */
    public void setView(ClickFragment view) {
        this.view = view;
    }

    /**
     * Обновляем кеш и значения
     */
    public void refresh() {
        //Обновляем значения в кеше
        value = preferences.getValue();
        stepValue = preferences.getStepValue();
        maxValue = preferences.getMaxValue();

        //Устанавливаем текущее значение счетчика
        view.setValue(value);
    }

    /**
     * Обработка нажатия
     */
    public void click() {
        //Увеличиваем значение
        value = value.add(stepValue);

        //Сбрасываем по достижении макс значения
        if (value.compareTo(maxValue) == 1)
            value = DefaultSettings.DEFAULT_VALUE;

        //Сохраняем
        preferences.setValue(value);

        //Обновляем значение
        view.setValue(value);
    }

    /**
     * Открываем настройки
     */
    public void settings() {
        //Показыываем фрагмент настроек
        activity.showFragment(R.id.mainFrameLayout, SettingsFragment_.class);
    }
}
