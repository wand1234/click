package ru.test.click.ui.activity.main;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import ru.test.click.R;
import ru.test.click.ui.activity.base.BaseActivity;
import ru.test.click.ui.fragment.click.ClickFragment_;

@EBean
public class MainPresenterImp {

    @RootContext
    protected BaseActivity activity;

    /**
     * Показываем фрагменты
     */
    public void showFragment() {
        //Показыываем фрагмент нажатия
        activity.showFragment(R.id.mainFrameLayout, ClickFragment_.class);
    }
}
