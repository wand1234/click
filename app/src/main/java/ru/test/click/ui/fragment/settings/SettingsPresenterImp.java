package ru.test.click.ui.fragment.settings;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.StringRes;

import ru.test.click.R;
import ru.test.click.preferences.shared.SharedPreferences;
import ru.test.click.settings.DefaultSettings;
import ru.test.click.ui.activity.base.BaseActivity;
import ru.test.click.ui.fragment.click.ClickFragment_;

@EBean
public class SettingsPresenterImp {

    @RootContext
    protected BaseActivity activity;

    /**
     * Представление
     */
    private SettingsFragment view;

    @Bean
    protected SharedPreferences preferences;

    @StringRes(R.string.error)
    protected String error;

    /**
     * Устанавливаем представление
     * @param view представление
     */
    public void setView(SettingsFragment view) {
        this.view = view;
    }

    /**
     * Показываем фрагмент кликов
     */
    private void showClick() {
        activity.showFragment(R.id.mainFrameLayout, ClickFragment_.class);
    }

    /**
     * Получаем настройки из базы и обновляем значения на экране
     */
    public void refresh() {
        view.setStepValue(preferences.getStepValue());
        view.setMaxValue(preferences.getMaxValue());
    }

    /**
     * Созраняем настройки
     */
    public void save() {
        if (view.getStepValue() != null &&
                view.getMaxValue() != null) {

            //Сохряняем
            preferences.setStepValue(view.getStepValue());
            preferences.setMaxValue(view.getMaxValue());

            //Показываем фрагмент кликов
            showClick();
        } else {
            activity.showMessage(error);
        }
    }

    /**
     * Сбрасываем настройки
     */
    public void reset() {
        //Сбрасываем значение
        preferences.setValue(DefaultSettings.DEFAULT_VALUE);

        //Показываем фрагмент кликов
        showClick();
    }

    /**
     * Закрываем фрагмент
     */
    public void back() {
        //Показываем фрагмент кликов
        showClick();
    }
}
