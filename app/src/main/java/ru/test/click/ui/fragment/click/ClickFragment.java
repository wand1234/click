package ru.test.click.ui.fragment.click;

import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.math.BigDecimal;

import ru.test.click.R;
import ru.test.click.ui.animations.ClickAnimation;
import ru.test.click.ui.fragment.base.BaseFragment;

@EFragment(R.layout.fragment_click)
public class ClickFragment extends BaseFragment {

    @Bean
    protected ClickPresenterImp presenter;

    @ViewById(R.id.click_info)
    protected TextView clickInfo;

    /**
     * Выводим новое значение
     * @param value новое значение
     */
    public void setValue(BigDecimal value) {
        clickInfo.setText(value.toString());
    }

    @AfterViews
    protected void show() {
        //Устанавливаем представление
        presenter.setView(this);

        //Обновляем настройки
        presenter.refresh();
    }

    @Click(R.id.click_info)
    protected void click() {
        presenter.click();
    }

    @Click(R.id.settings)
    protected void settings(View view) {
        view.startAnimation(
                new ClickAnimation().setEndRunnable(() -> presenter.settings())
        );
    }
}
