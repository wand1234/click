package ru.test.click.ui.activity.main;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NonConfigurationInstance;

import ru.test.click.R;
import ru.test.click.ui.activity.base.BaseActivity;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @NonConfigurationInstance
    @Bean
    protected MainPresenterImp presenter;

    /**
     * Показываем фрагменты
     */
    @AfterViews
    protected void show() {
        presenter.showFragment();
    }
}
