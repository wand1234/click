package ru.test.click.ui.activity.base;

import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.res.ColorRes;

import ru.test.click.R;

@EBean
public abstract class BaseActivity extends AppCompatActivity {

    /**
     * Цвет сообщений
     */
    @ColorRes(R.color.white)
    protected int messageTextColor;

    /**
     * Меняем фрагмент
     * @param id идентификатор контейнера
     * @param fragmentClass класс фрагмента
     * @return фрагмент
     */
    public Fragment showFragment(@IdRes int id, Class fragmentClass) {
        String tag = fragmentClass.getSimpleName();
        Fragment fragment;
        try {
            fragment = (Fragment) fragmentClass.getConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(id, fragment, tag)
                .commit();

        return fragment;
    }

    /**
     * Выводим сообщение
     * @param msg сообщение
     */
    public void showMessage(String msg) {
        View parentView = this.findViewById(android.R.id.content);
        TSnackbar snackbar = TSnackbar.make(parentView, msg, Snackbar.LENGTH_LONG);
        TextView errorText = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        errorText.setTextColor(messageTextColor);
        snackbar.show();
    }
}
