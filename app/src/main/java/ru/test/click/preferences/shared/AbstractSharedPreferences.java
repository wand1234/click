package ru.test.click.preferences.shared;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public abstract class AbstractSharedPreferences {

    /**
     * Получить строковое значение
     * @param context контекст
     * @param name имя параметра
     * @param defValue значение по умолчанию
     * @return
     */
    protected String getString(Context context, String name, String defValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            String value = preferences.getString(name, defValue);
            if (value!=null && value.length()>0) {
                return value;
            } else {
                //Устанавливаем значение по умолчанию
                setString(context, name, defValue);
                return defValue;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return defValue;
    }

    /**
     * Установить строковое значение
     * @param context сонтекст
     * @param name имя параметра
     * @param value значение
     */
    protected void setString(Context context, String name, String value) {
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor preferencesEditor = preferences.edit();

            preferencesEditor.putString(name, value);

            //Сохраняем
            preferencesEditor.apply();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
