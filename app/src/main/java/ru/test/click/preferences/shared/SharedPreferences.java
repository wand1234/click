package ru.test.click.preferences.shared;

import android.content.Context;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.StringRes;

import java.math.BigDecimal;

import ru.test.click.R;
import ru.test.click.settings.DefaultSettings;

@EBean
public class SharedPreferences extends AbstractSharedPreferences {

    @RootContext
    protected Context context;

    @StringRes(R.string.preference_value_key)
    protected String preference_value_key;

    @StringRes(R.string.preference_step_value_key)
    protected String preference_step_value_key;

    @StringRes(R.string.preference_max_value_key)
    protected String preference_max_value_key;

    /**
     * Получить текущее значение счетчика
     * @return значение
     */
    public BigDecimal getValue() {
        return new BigDecimal(getString(context, preference_value_key, DefaultSettings.DEFAULT_VALUE.toString()));
    }

    /**
     * Установить значение счетчика
     * @param value новое значение
     */
    public void setValue(BigDecimal value) {
        setString(context, preference_value_key, value.toString());
    }

    /**
     * Получить шаг
     * @return значение шага
     */
    public BigDecimal getStepValue() {
        return new BigDecimal(getString(context, preference_step_value_key, DefaultSettings.DEFAULT_STEP_VALUE.toString()));
    }

    /**
     * Задать шаг
     * @param value новое значение
     */
    public void setStepValue(BigDecimal value) {
        setString(context, preference_step_value_key, value.toString());
    }

    /**
     * Получить максимальное значение счетчика
     * @return значение
     */
    public BigDecimal getMaxValue() {
        return new BigDecimal(getString(context, preference_max_value_key, DefaultSettings.DEFAULT_MAX_VALUE.toString()));
    }

    /**
     * Установить максимальное значение счетчика
     * @param value значение
     */
    public void setMaxValue(BigDecimal value) {
        setString(context, preference_max_value_key, value.toString());
    }
}
