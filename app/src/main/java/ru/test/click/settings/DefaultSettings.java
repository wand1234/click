package ru.test.click.settings;

import java.math.BigDecimal;

public interface DefaultSettings {

    /**
     * Значение по умолчанию для счетчика
     */
    BigDecimal DEFAULT_VALUE = BigDecimal.ZERO;

    /**
     * Значение по умолчанию для шага счетчика
     */
    BigDecimal DEFAULT_STEP_VALUE = BigDecimal.ONE;

    /**
     * Значение по умолчанию для максималього значения
     */
    BigDecimal DEFAULT_MAX_VALUE = new BigDecimal(1000);

    /**
     * Время показа сообщений
     */
    Integer MESSAGE_DIALOG_TIMEOUT = 15 * 1000;
}
