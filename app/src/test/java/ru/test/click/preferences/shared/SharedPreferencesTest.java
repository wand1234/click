package ru.test.click.preferences.shared;

import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.math.BigDecimal;

import ru.test.click.BuildConfig;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class SharedPreferencesTest {

    private SharedPreferences sharedPreferences;

    @Before
    public void setUp() throws Exception {
        sharedPreferences = new SharedPreferences();
        sharedPreferences.context = RuntimeEnvironment.application;
    }

    @Test
    public void getValue() throws Exception {
        sharedPreferences.setValue(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, sharedPreferences.getValue());
    }

    @Test
    public void setValue() throws Exception {
        sharedPreferences.setValue(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, sharedPreferences.getValue());
    }

    @Test
    public void getStepValue() throws Exception {
        sharedPreferences.setStepValue(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, sharedPreferences.getStepValue());
    }

    @Test
    public void setStepValue() throws Exception {
        sharedPreferences.setStepValue(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, sharedPreferences.getStepValue());
    }

    @Test
    public void getMaxValue() throws Exception {
        sharedPreferences.setMaxValue(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, sharedPreferences.getMaxValue());
    }

    @Test
    public void setMaxValue() throws Exception {
        sharedPreferences.setMaxValue(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, sharedPreferences.getMaxValue());
    }
}