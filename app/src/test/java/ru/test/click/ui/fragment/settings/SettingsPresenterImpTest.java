package ru.test.click.ui.fragment.settings;

import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.math.BigDecimal;

import ru.test.click.BuildConfig;
import ru.test.click.preferences.shared.SharedPreferences;
import ru.test.click.ui.activity.main.MainActivity;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class SettingsPresenterImpTest {

    private SettingsFragment view;
    private SettingsPresenterImp settingsPresenterImp;

    @Before
    public void setUp() throws Exception {
        settingsPresenterImp = new SettingsPresenterImp();
        settingsPresenterImp.preferences = Mockito.mock(SharedPreferences.class);
        view = Mockito.mock(SettingsFragment.class);
        settingsPresenterImp.setView(view);
        settingsPresenterImp.activity = Mockito.mock(MainActivity.class);

        Mockito.when(settingsPresenterImp.preferences.getValue()).thenReturn(BigDecimal.ZERO);
        Mockito.when(settingsPresenterImp.preferences.getStepValue()).thenReturn(BigDecimal.ONE);
        Mockito.when(settingsPresenterImp.preferences.getMaxValue()).thenReturn(BigDecimal.TEN);

        Mockito.doAnswer(invocation -> {
            BigDecimal value = invocation.getArgument(0);
            Mockito.when(settingsPresenterImp.preferences.getValue()).thenReturn(value);
            return null;
        }).when(settingsPresenterImp.preferences).setValue(Mockito.any(BigDecimal.class));

        Mockito.doAnswer(invocation -> {
            BigDecimal value = invocation.getArgument(0);
            Mockito.when(settingsPresenterImp.preferences.getStepValue()).thenReturn(value);
            return null;
        }).when(settingsPresenterImp.preferences).setStepValue(Mockito.any(BigDecimal.class));

        Mockito.doAnswer(invocation -> {
            BigDecimal value = invocation.getArgument(0);
            Mockito.when(settingsPresenterImp.preferences.getMaxValue()).thenReturn(value);
            return null;
        }).when(settingsPresenterImp.preferences).setMaxValue(Mockito.any(BigDecimal.class));
    }

    @Test
    public void refresh() throws Exception {
        settingsPresenterImp.refresh();
        assertEquals(BigDecimal.ONE, settingsPresenterImp.preferences.getStepValue());
        assertEquals(BigDecimal.TEN, settingsPresenterImp.preferences.getMaxValue());
    }

    @Test
    public void save() throws Exception {
        Mockito.when(view.getStepValue()).thenReturn(BigDecimal.TEN);
        Mockito.when(view.getMaxValue()).thenReturn(BigDecimal.TEN);
        settingsPresenterImp.save();
        assertEquals(BigDecimal.TEN, settingsPresenterImp.preferences.getStepValue());
        assertEquals(BigDecimal.TEN, settingsPresenterImp.preferences.getMaxValue());
    }

    @Test
    public void reset() throws Exception {
        settingsPresenterImp.reset();
        assertEquals(BigDecimal.ZERO, settingsPresenterImp.preferences.getValue());
    }
}