package ru.test.click.ui.fragment.click;

import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.math.BigDecimal;

import ru.test.click.BuildConfig;
import ru.test.click.preferences.shared.SharedPreferences;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
public class ClickPresenterImpTest {

    private ClickPresenterImp clickPresenterImp;

    @Before
    public void setUp() throws Exception {
        clickPresenterImp = new ClickPresenterImp();
        clickPresenterImp.preferences = Mockito.mock(SharedPreferences.class);
        clickPresenterImp.setView(Mockito.mock(ClickFragment.class));

        Mockito.when(clickPresenterImp.preferences.getValue()).thenReturn(BigDecimal.ZERO);
        Mockito.when(clickPresenterImp.preferences.getStepValue()).thenReturn(BigDecimal.ONE);
        Mockito.when(clickPresenterImp.preferences.getMaxValue()).thenReturn(BigDecimal.TEN);

        Mockito.doAnswer(invocation -> {
            BigDecimal value = invocation.getArgument(0);
            Mockito.when(clickPresenterImp.preferences.getValue()).thenReturn(value);
            return null;
        }).when(clickPresenterImp.preferences).setValue(Mockito.any(BigDecimal.class));
    }

    @Test
    public void refresh() throws Exception {
        clickPresenterImp.refresh();
        assertEquals(BigDecimal.ZERO, clickPresenterImp.preferences.getValue());
    }

    @Test
    public void click() throws Exception {
        clickPresenterImp.refresh();
        clickPresenterImp.click();
        assertEquals(BigDecimal.ONE, clickPresenterImp.preferences.getValue());
    }
}