package ru.test.click.ui.fragment.click;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import ru.test.click.R;
import ru.test.click.ui.activity.main.MainActivity_;

public class ClickFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity_> mActivityRule = new ActivityTestRule<>(
            MainActivity_.class);

    @Test
    public void setup() throws Exception {
        Espresso.onView(ViewMatchers.withId(R.id.settings)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.step_value)).perform(ViewActions.replaceText("1"));
        Espresso.onView(ViewMatchers.withId(R.id.max_value)).perform(ViewActions.replaceText("100"));
        Espresso.onView(ViewMatchers.withId(R.id.save)).perform(ViewActions.click());
    }

    @Test
    public void reset() throws Exception {
        Espresso.onView(ViewMatchers.withId(R.id.settings)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.reset)).perform(ViewActions.click());
    }

    @Test
    public void click() throws Exception {
        Espresso.onView(ViewMatchers.withId(R.id.click_info)).check(ViewAssertions.matches(ViewMatchers.withText("0")));
        Espresso.onView(ViewMatchers.withId(R.id.click_info)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.click_info)).check(ViewAssertions.matches(ViewMatchers.withText("1")));
    }
}